import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./components/navbar/Navbar";
import Home from "./components/home/Home";
import Skills from "./components/skills/Skills";
import Particles from "react-particles-js";

const App = () => {
  return (
    <Router>
      <div className='ma2web'>
        <Particles />
        <div className='navbar'>
          <Navbar />
        </div>
        <Switch>
          <div className='contents'>
            <Route path='/' component={Home} exact />
            <Route path='/skills' component={Skills} exact />
          </div>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
