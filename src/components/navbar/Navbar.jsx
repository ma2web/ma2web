import React from "react";
import { Link, NavLink } from "react-router-dom";
import Avatar from "../avatar/Avatar";
import { MdFileDownload, MdPictureAsPdf } from "react-icons/md";

const Navbar = () => {
  return (
    <div>
      <div>
        <Avatar />
      </div>
      <div className='navbar-items'>
        <nav>
          <ul>
            <li>
              <NavLink to='/' exact>
                Home
              </NavLink>
            </li>
            <li>
              <NavLink to='/skills' exact>
                Skills
              </NavLink>
            </li>
            <li>
              <a className='download-resume'>
                <MdPictureAsPdf /> PDF Resume
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default Navbar;
