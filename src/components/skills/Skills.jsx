import React, { useState } from "react";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import ProgressProvider from "./ProgressProvider";

const Skills = () => {
  const skills = [
    {
      name: "MongoDB",
      value: 38,
    },
    {
      name: "Express",
      value: 41,
    },
    {
      name: "ReactJS",
      value: 84,
    },
    {
      name: "React Native",
      value: 30,
    },
    {
      name: "NodeJS",
      value: 35,
    },
    {
      name: "JavaScript",
      value: 65,
    },
    {
      name: "Electron",
      value: 35,
    },
    {
      name: "PWA",
      value: 35,
    },
    {
      name: "TypeScript",
      value: 25,
    },
    {
      name: "Wordpress",
      value: 90,
    },
    {
      name: "HTML",
      value: 90,
    },
    {
      name: "CSS",
      value: 90,
    },
    {
      name: "AE",
      value: 40,
    },
    {
      name: "PS",
      value: 70,
    },
    {
      name: "Ai",
      value: 30,
    },
    {
      name: "XD",
      value: 60,
    },
  ];
  // const [value_end, set_value_end] = useState(0);

  // setTimeout(() => {
  //   set_value_end("73");
  // }, 500);

  return (
    <div className='skills-container'>
      <div className='skills'>
        {skills.map((item, i) => {
          return (
            <div
              className='skill'
              style={{ animation: `contents 300ms forwards ${i / 2}s` }}>
              <div>
                <ProgressProvider
                  valueStart={0}
                  valueEnd={parseInt(item.value)}>
                  {(value) => (
                    <CircularProgressbar
                      value={value}
                      text={`${value}%`}
                      styles={buildStyles({
                        strokeLinecap: "round",
                        textSize: "16px",
                        pathTransitionDuration: 1.5,
                        pathColor: "#fff",
                        textColor: "#fff",
                        trailColor: "transparent",
                        backgroundColor: "#fff",
                      })}
                    />
                  )}
                </ProgressProvider>
              </div>
              <div>
                <h3>{item.name}</h3>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Skills;
