import React from "react";
// import avatar from "../../assets/img/avatar.png";
import avatar from "../../assets/img/ma2web.jpeg";

const Avatar = () => {
  return (
    <div className='avatar'>
      <div>
        <img src={avatar} alt='avatar' />
      </div>
      <div>
        <h1>MA2WEB</h1>
        <h2>JS Developer</h2>
      </div>
    </div>
  );
};

export default Avatar;
